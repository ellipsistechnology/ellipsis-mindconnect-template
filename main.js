const { initAgents, startScanning } = require('ellipsis-mindconnect')
const { etongueToJson } = require('./etongue.js')

initAgents({
    "scanInterval": 60000,
    "assets": [
        // {
        //     "name": "etongue",
        //     "id": "86f361a751e249aaa893009981b403fa",
        //     "dataMap": callback, // callback must be a function that will take a string and convert it to JSON
        //     "onboardingKey": {  "content": {    "baseUrl": "https://southgate.eu1.mindsphere.io",    "iat": "eyJraWQiOiJrZXktaWQtMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJTQ0kiLCJzdWIiOiIyYTlhYjgxMTVkM2M0MGViOTg3NzZkMzFmMjA0NTMxZSIsImF1ZCI6IkFJQU0iLCJpYXQiOjE2MDM5NjAxMjYsIm5iZiI6MTYwMzk2MDEyNiwiZXhwIjoxNjA0NTY0OTI2LCJqdGkiOiI4ZWY2NjU4MS00MTcyLTQ5MmQtYWVmMS00NDFiZDA4ZjhkYTkiLCJzY29wZSI6IklBVCIsInRlbiI6InV0YXNpb3QiLCJ0ZW5fY3R4IjoibWFpbi10ZW5hbnQiLCJjbGllbnRfY3JlZGVudGlhbHNfcHJvZmlsZSI6WyJTSEFSRURfU0VDUkVUIl0sInNjaGVtYXMiOlsidXJuOnNpZW1lbnM6bWluZHNwaGVyZTp2MSJdfQ.YCHkyWd5qrmi-9rhBw9dZo2tLoTvT2e6eCE1kMKnkeLPGUbrTGM5NSgttnLOZnTzHC4I5OTbMgOO5ya66eAaFdaxFA56g3j35m1jgM1fauEotnX_wqvPrWKYNyUw-6gwczc6Hh3tXoisSVJR7hY0ENxAHLmpgXUih4rttakyXgTMtEKdK6bZ84ITdI24cJ7A7u9o8WMtK_cTT9MkcIijjv0AFTXFvYVghs8h-qok379_6o_6sVY9WcBMI-dZ9QdbPhl-J4DWmWH1O8y7ck6orIWtPtlLTNf_gfPW9feqvom2KQnz-HhlSoByMnuRR9CEXYC3le4aXmG9lENV_a4L1A",    "clientCredentialProfile": [      "SHARED_SECRET"    ],    "clientId": "2a9ab8115d3c40eb98776d31f204531e",    "tenant": "utasiot"  },  "expiration": "2020-11-05T08:28:46.000Z"}
        // }
    ]
})
.then(startScanning)
.catch(console.error)
